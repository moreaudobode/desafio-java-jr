package selecao.stefanini.cpg.service;

import java.io.Serializable;

import javax.inject.Inject;

import selecao.stefanini.cpg.model.Obra;
import selecao.stefanini.cpg.repository.Obras;
import selecao.stefanini.cpg.util.Transacional;

public class CadastroObraService implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Obras obras;
	
	@Transacional
	public void salvar(Obra obra) {
		obras.guardar(obra);
	}
	
	@Transacional
	public void excluir(Obra obra) {
		obras.remover(obra);
	}

}


