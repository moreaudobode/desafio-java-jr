package selecao.stefanini.cpg.service;

import java.io.Serializable;

import javax.inject.Inject;

import selecao.stefanini.cpg.model.Autor;
import selecao.stefanini.cpg.repository.Autores;
import selecao.stefanini.cpg.util.Transacional;



public class CadastroAutorService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private Autores autores;

    @Transacional
    public void salvar(Autor autor) {
        autores.guardar(autor);
    }

    @Transacional
    public void excluir(Autor autor) {
        autores.remover(autor);
    }

}

