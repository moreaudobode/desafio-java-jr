package selecao.stefanini.cpg.repository;

import java.io.Serializable;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import java.util.List;
import selecao.stefanini.cpg.model.Autor;
import selecao.stefanini.cpg.model.Obra;

public class Autores implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Autores() {

	}

	public Autores(EntityManager manager) {
		this.manager = manager;
	}

	public Autor porId(Long id) {
		return manager.find(Autor.class, id);
	}

	public List<Autor> pesquisar(String nome) {
		String jpql = "from Autor where razaoSocial like :razaoSocial";
		
		TypedQuery<Autor> query = manager
				.createQuery(jpql, Autor.class);
		
		query.setParameter("razaoSocial", nome + "%");
		
		return query.getResultList();
	}
	
	public List<Autor> todos() {
         return manager.createQuery("from Autor", Autor.class).getResultList();
    }
	
	public List<Obra> todasObras() {
        return manager.createQuery("from Obra o, Autor a, Obra_Autor oa where o.id = oa.obra_id and a.id = oa.autoreslista_id ", Obra.class).getResultList();
   }

	public Autor guardar(Autor autor) {
		return manager.merge(autor);
	}

	public void remover(Autor autor) {
		autor = porId(autor.getId());
		manager.remove(autor);
	}
}