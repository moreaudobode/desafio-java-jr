package selecao.stefanini.cpg.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import selecao.stefanini.cpg.model.Autor;
import selecao.stefanini.cpg.model.Obra;

public class Obras implements Serializable {


	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public Obras() {
		
	}
	
	public Obras(EntityManager manager) {
		this.manager = manager;
	}
	
	public Obra porId(Long id) {
		return manager.find(Obra.class, id);
	}
	
	public List<Obra> pesquisar(String nome) {
		String jpql = "from Obra where nome like :nomeObra";
		
		TypedQuery<Obra> query = manager
				.createQuery(jpql, Obra.class);
		
		query.setParameter("nomeObra", nome + "%");
		
		return query.getResultList();
	}
	
	public List<Obra> pesquisarDescricao(String descricao) {
		String jpql = "from Obra where descricao like :descricaoObra";
		
		TypedQuery<Obra> query = manager
				.createQuery(jpql, Obra.class);
		
		query.setParameter("descricaoObra", descricao + "%");
		
		return query.getResultList();
	}
	
	public List<Obra> todas() {
         return manager.createQuery("from Obra", Obra.class).getResultList();
    }
	
	public List<Autor> todosAutores() {
        return manager.createQuery("from Obra o, Autor a, Obra_Autor oa where o.id = oa.obra_id and a.id = oa.autoreslista_id ", Autor.class).getResultList();
   }

	public Obra guardar(Obra obra) {
		return manager.merge(obra);
	}

	public void remover(Obra obra) {
		obra = porId(obra.getId());
		manager.remove(obra);
	}
}


