package selecao.stefanini.cpg.controller;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import selecao.stefanini.cpg.model.Autor;
import selecao.stefanini.cpg.repository.Autores;
import selecao.stefanini.cpg.service.CadastroAutorService;
import selecao.stefanini.cpg.util.FacesMessages;

@Named
@ViewScoped
public class GestaoAutoresBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private Autores autores;
    
    @Inject
    private FacesMessages messages;
    
    
    @Inject
    private CadastroAutorService cadastroAutorService;
    
    private List<Autor> listaAutores;
    
    private String termoPesquisa;
    
       
    private Autor autor;
    
    public void prepararNovoAutor() {
        autor = new Autor();
    }
    
    
    public void salvar() {
        cadastroAutorService.salvar(autor);
        
        atualizarRegistros();
        
        messages.info("Autor salva com sucesso!");
        
        RequestContext.getCurrentInstance().update(Arrays.asList(
                "frm:autoresDataTable", "frm:messages"));
    }
    
    public void excluir() {
        cadastroAutorService.excluir(autor);
        
        autor = null;
        
        atualizarRegistros();
        
        messages.info("Autor excluída com sucesso!");
    }
    
    public void pesquisar() {
        listaAutores = autores.pesquisar(termoPesquisa);
        
        if (listaAutores.isEmpty()) {
            messages.info("Sua consulta não retornou registros.");
        }
    }
    
    public void todosAutores() {
        listaAutores = autores.todos();
    }
    
    
    private void atualizarRegistros() {
        if (jaHouvePesquisa()) {
            pesquisar();
        } else {
            todosAutores();
        }
    }
    
    private boolean jaHouvePesquisa() {
        return termoPesquisa != null && !"".equals(termoPesquisa);
    }
    
    public List<Autor> getlistaAutores() {
        return listaAutores;
    }
    
    public String getTermoPesquisa() {
        return termoPesquisa;
    }
    
    public void setTermoPesquisa(String termoPesquisa) {
        this.termoPesquisa = termoPesquisa;
    }
    
      
    public Autor getAutor() {
        return autor;
    }
    
    public void setAutor(Autor autor) {
        this.autor = autor;
    }
    
    public boolean isAutorSeleciona() {
        return autor != null && autor.getId() != null;
    }
}