package selecao.stefanini.cpg.controller;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import selecao.stefanini.cpg.model.Obra;
import selecao.stefanini.cpg.repository.Obras;
import selecao.stefanini.cpg.service.CadastroObraService;
import selecao.stefanini.cpg.util.FacesMessages;

@Named
@ViewScoped
public class GestaoObrasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Obras obras;

	@Inject
	private FacesMessages messages;
	
	 @Inject
	 private CadastroObraService cadastroObraService;

	private List<Obra> listaObras;

	private String termoPesquisa;

	private Obra obra;

	public void prepararNovaObra() {
		obra = new Obra();
	}

	public void salvar() {
		cadastroObraService.salvar(obra);

		atualizarRegistros();

		messages.info("Obra salva com sucesso!");

		RequestContext.getCurrentInstance().update(Arrays.asList("frm:obrasDataTable", "frm:messages"));
	}

	public void excluir() {
		cadastroObraService.excluir(obra);

		obra = null;

		atualizarRegistros();

		messages.info("Obra excluída com sucesso!");
	}

	public void pesquisar() {
		listaObras = obras.pesquisar(termoPesquisa);

		if (listaObras.isEmpty()) {
			messages.info("Sua consulta não retornou registros.");
		}
	}

	public void todasObras() {
		listaObras = obras.todas();
	}

	private void atualizarRegistros() {
		if (jaHouvePesquisa()) {
			pesquisar();
		} else {
			todasObras();
		}
	}

	private boolean jaHouvePesquisa() {
		return termoPesquisa != null && !"".equals(termoPesquisa);
	}

	public List<Obra> getListaObras() {
		return listaObras;
	}

	public String getTermoPesquisa() {
		return termoPesquisa;
	}

	public void setTermoPesquisa(String termoPesquisa) {
		this.termoPesquisa = termoPesquisa;
	}
	
    public Obra getObra() {
        return obra;
    }
    
    public void setObra(Obra obra) {
        this.obra = obra;
    }
    
    public boolean isObraSeleciona() {
        return obra != null && obra.getId() != null;
    }

}
